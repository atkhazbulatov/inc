FROM tecktron/python-bjoern:python-3.11-slim
RUN pip install falcon==3.1.3 peewee==3.17.0
COPY src/ /app
