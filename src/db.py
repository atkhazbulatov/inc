import peewee
import playhouse.db_url

db = playhouse.db_url.connect('sqlite:///:memory:')


class Entity(peewee.Model):
    class Meta:
        database = db


def create_tables():
	db.connect()
	db.create_tables(Entity.__subclasses__())
