import falcon


class NumberAlreadyExistsError(falcon.HTTPBadRequest):
    def __init__(self, num):
        super().__init__('Number Already Exists', f'The number {num} already exists')


class IncrementAlreadyExistsError(falcon.HTTPBadRequest):
    def __init__(self, num):
        super().__init__('Increment Already Exists', f'The increment of number {num} already exists')
