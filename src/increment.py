import peewee

import error
import model


class IncrementResource:
    def on_post(self, req, resp):
        num = req.get_param_as_int('num', required=True)

        if model.Num.get_or_none(model.Num.pk == num + 1):
            raise error.IncrementAlreadyExistsError(num)

        try:
            model.Num.create(pk = num)
        except peewee.IntegrityError:
            raise error.NumberAlreadyExistsError(num)

        resp.media = {'num': num + 1}
