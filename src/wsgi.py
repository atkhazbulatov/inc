import falcon

import db
import increment


def make_wsgi_app():
    db.create_tables()
    api = falcon.API()
    api.req_options.auto_parse_form_urlencoded = True
    api.add_route('/inc', increment.IncrementResource())
    return api


application = make_wsgi_app()
